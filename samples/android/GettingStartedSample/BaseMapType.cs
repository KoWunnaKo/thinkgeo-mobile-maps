namespace GettingStartedSample
{
    public enum BaseMapType
    {
        ThinkGeoCloudMapLight = 0,
        ThinkGeoCloudMapAerial = 1,
        ThinkGeoCloudMapBybrid = 2,
    }
}