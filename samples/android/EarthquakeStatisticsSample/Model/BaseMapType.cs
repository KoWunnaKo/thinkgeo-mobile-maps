namespace MapSuiteEarthquakeStatistics
{
    public enum BaseMapType
    {
        ThinkGeoCloudLightMap = 0,
        ThinkGeoCloudAerialMap = 1,
        ThinkGeoCloudHybridMap = 2,
        OpenStreetMap = 3,
        BingMapsAerial = 4,
        BingMapsRoad = 5
    }
}