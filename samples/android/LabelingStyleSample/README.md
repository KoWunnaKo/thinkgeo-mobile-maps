# Labeling Style Sample for Android

### Description

This samples shows you how to examine different labeling techniques to make your maps informative.

![Screenshot](https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/raw/master/samples/android/LabelingStyleSample/ScreenShot.png)

### Requirements

This sample makes use of the following NuGet packages:

[ThinkGeo Core](https://www.nuget.org/packages/ThinkGeo.Core)

[ThinkGeo UI for Android](https://www.nuget.org/packages/ThinkGeo.UI.Android)

### ThinkGeo UI for Android Resources

[ThinkGeo UI for Android - Quickstart and API Docs](https://docs.thinkgeo.com/products/mobile-maps/v12.0/quickstart/#quick-start-display-a-simple-map-on-android)

[ThinkGeo UI for Android - Product Page](https://www.thinkgeo.com/mobile-maps)

### Additional Resources

[ThinkGeo Community Forums](http://community.thinkgeo.com/)

[ThinkGeo Website](https://www.thinkgeo.com/)

### About ThinkGeo

ThinkGeo is a GIS (Geographic Information Systems) company founded in 2004 and located in Frisco, TX. Our clients are in more than 40 industries including agriculture, energy, transportation, government, engineering, software development, and defense.
