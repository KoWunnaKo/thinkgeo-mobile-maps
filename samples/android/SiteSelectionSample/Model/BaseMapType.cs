namespace MapSuiteSiteSelection
{
    public enum BaseMapType
    {
        ThinkGeoCloudMapLight = 0,
        ThinkGeoCloudMapAerial = 1,
        ThinkGeoCloudMapHybrid = 2,
        OpenStreetMap = 3,
        BingMapsAerial = 4,
        BingMapsRoad = 5
    }
}