namespace MapSuiteEarthquakeStatistics
{
    public enum BaseMapType
    {
        ThinkGeoCloudMapsLight = 0,
        ThinkGeoCloudMapsDark = 1,
    }
}