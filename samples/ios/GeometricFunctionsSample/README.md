# Geometric Functions Sample for iOS

### Description

This samples shows you how to explore all of the different Geometric Functions APIs.

![Screenshot](https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/raw/master/samples/ios/GeometricFunctionsSample/ScreenShot.gif)

### Requirements

This sample makes use of the following NuGet packages:

[ThinkGeo Core](https://www.nuget.org/packages/ThinkGeo.Core)

[ThinkGeo UI for iOS](https://www.nuget.org/packages/ThinkGeo.UI.iOS)

### ThinkGeo UI for iOS Resources

[ThinkGeo UI for iOS - Quickstart and API Docs](https://docs.thinkgeo.com/products/mobile-maps/v12.0/quickstart/#quick-start-display-a-simple-map-on-ios)

[ThinkGeo UI for iOS - Product Page](https://www.thinkgeo.com/mobile-maps)

### Additional Resources

[ThinkGeo Community Forums](http://community.thinkgeo.com/)

[ThinkGeo Website](https://www.thinkgeo.com/)

### About ThinkGeo

ThinkGeo is a GIS (Geographic Information Systems) company founded in 2004 and located in Frisco, TX. Our clients are in more than 40 industries including agriculture, energy, transportation, government, engineering, software development, and defense.
