# MapDoubleTapMode

**Summary**

N/A

**Remarks**

N/A

**Items**

|Name|Description|
|---|---|
|Default|Default value (ZoomIn) of the enumeration.|
|ZoomIn|This enumeration item means that the map will ZoomIn when a double tap occurs.|
|Disabled|This enumeration item means that the ZoomIn functionality for double tap will be disabled.|

