# ProcessOtherOverlaysMode

**Summary**

N/A

**Remarks**

N/A

**Items**

|Name|Description|
|---|---|
|ProcessOtherOverlays|This enumeration item means that other overlays will still be executed.|
|DoNotProcessOtherOverlays|This enumeration item means that other overlays will be ignored.|
|Default|Default value of the enumeration, the same as ProcessOtherOverlays.|

