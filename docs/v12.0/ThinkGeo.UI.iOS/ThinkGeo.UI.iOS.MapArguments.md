# MapArguments


## Inheritance Hierarchy

+ `Object`
  + **`MapArguments`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`MapArguments()`](#maparguments)|
|[`MapArguments(RectangleShape,GeographyUnit,Single,Single)`](#mapargumentsrectangleshapegeographyunitsinglesingle)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`CurrentExtent`](#currentextent)|[`RectangleShape`](../ThinkGeo.Core/ThinkGeo.Core.RectangleShape.md)|Gets or sets the current extent.|
|[`CurrentResolution`](#currentresolution)|`Double`|Gets or sets a double value that indicates the current resolution of the map.|
|[`CurrentScale`](#currentscale)|`Double`|Gets or sets a double value that indicates the current scale of the map.|
|[`CustomData`](#customdata)|Dictionary<`String`,`Object`>|Gets the custom data.|
|[`MapUnit`](#mapunit)|[`GeographyUnit`](../ThinkGeo.Core/ThinkGeo.Core.GeographyUnit.md)|Gets or sets the GeographyUnit for the map.|
|[`MaximumScale`](#maximumscale)|`Double`|N/A|
|[`MinimumScale`](#minimumscale)|`Double`|N/A|
|[`ScaleFactor`](#scalefactor)|`Single`|Gets or sets the scale factor, this is a value number of device pixels per logical coordinate point.|
|[`ScreenHeight`](#screenheight)|`nfloat`|Gets or sets the height of current map object in screen coordinate.|
|[`ScreenWidth`](#screenwidth)|`nfloat`|Gets or sets the width of current map object in screen coordinate.|
|[`ZoomLevelSet`](#zoomlevelset)|[`ZoomLevelSet`](../ThinkGeo.Core/ThinkGeo.Core.ZoomLevelSet.md)|Gets or sets the zoom level set.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`MapArguments()`](#maparguments)|
|[`MapArguments(RectangleShape,GeographyUnit,Single,Single)`](#mapargumentsrectangleshapegeographyunitsinglesingle)|

### Protected Constructors


### Public Properties

#### `CurrentExtent`

**Summary**

   *Gets or sets the current extent.*

**Remarks**

   *N/A*

**Return Value**

[`RectangleShape`](../ThinkGeo.Core/ThinkGeo.Core.RectangleShape.md)

---
#### `CurrentResolution`

**Summary**

   *Gets or sets a double value that indicates the current resolution of the map.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `CurrentScale`

**Summary**

   *Gets or sets a double value that indicates the current scale of the map.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `CustomData`

**Summary**

   *Gets the custom data.*

**Remarks**

   *N/A*

**Return Value**

Dictionary<`String`,`Object`>

---
#### `MapUnit`

**Summary**

   *Gets or sets the GeographyUnit for the map.*

**Remarks**

   *N/A*

**Return Value**

[`GeographyUnit`](../ThinkGeo.Core/ThinkGeo.Core.GeographyUnit.md)

---
#### `MaximumScale`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `MinimumScale`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `ScaleFactor`

**Summary**

   *Gets or sets the scale factor, this is a value number of device pixels per logical coordinate point.*

**Remarks**

   *The coordinate space used by application developers is measured in logical points. High-resolution (Retina) displays will have more than a single physical pixel per logical point and this property specifies the scale factor.*

**Return Value**

`Single`

---
#### `ScreenHeight`

**Summary**

   *Gets or sets the height of current map object in screen coordinate.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `ScreenWidth`

**Summary**

   *Gets or sets the width of current map object in screen coordinate.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `ZoomLevelSet`

**Summary**

   *Gets or sets the zoom level set.*

**Remarks**

   *N/A*

**Return Value**

[`ZoomLevelSet`](../ThinkGeo.Core/ThinkGeo.Core.ZoomLevelSet.md)

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


